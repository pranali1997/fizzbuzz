package fizzBuzz;

public class FizzBuzz {

    public String convertNumber(int number) {
        boolean multipleOfThree = number % 3 == 0;
        boolean multipleOfFive = number % 5 == 0;
        String result="";
        if (multipleOfThree )
            result += "Fizz";

        if (multipleOfFive)
            result += "Buzz";

        if(result == "") {
            return String.valueOf(number);
        }
        return  result;
    }
}


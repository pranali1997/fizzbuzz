package fizzBuzz;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

public class FizzBuzzTest {
    @Test
    public void shouldReturnFizzWhenGivenNumberIsMultipleOfThree() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String convertedNumber = fizzBuzz.convertNumber(9);
        assertThat(convertedNumber,is("Fizz"));
   }

    @Test
    public void shouldReturnBuzzIfGivenNumberIsMultipleOfFive() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String convertedNumber = fizzBuzz.convertNumber(10);
        assertThat(convertedNumber,is("Buzz"));
    }

    @Test
    public void shouldReturnFizzBuzzIfGivenNumberIsMultipleOfThreeAndFive() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String convertedNumber = fizzBuzz.convertNumber(15);
        assertThat(convertedNumber,is("FizzBuzz"));
    }

    @Test
    public void shouldReturnGivenNumberIfGivenNumberIsNotMultipleOfFiveOrThree() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String convertedNumber = fizzBuzz.convertNumber(7);
        assertThat(convertedNumber,is("7"));
    }
}
